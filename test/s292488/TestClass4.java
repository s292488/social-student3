package s292488;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class TestClass4 {
	
Social s;
	
	@Before
	public void setUp() throws PersonExistsException, NoSuchCodeException {
		s = new Social();
		s.addPerson("NeverMaxine", "Max", "Caulfield");
		s.addPerson("TotallyNotChloe", "Chloe", "Price");
		s.addPerson("AcherRambel", "Rachel", "Amber");
		s.addPerson("FancyAWaffle", "Joyce", "Price-Madsen");
		s.addPerson("PrayUp", "Kate", "Marsh");
		s.addPerson("NerdChief", "Warren", "Graham");
		
		s.addFriendship("NeverMaxine", "TotallyNotChloe");
		s.addFriendship("AcherRambel", "TotallyNotChloe");
		s.addFriendship("TotallyNotChloe", "FancyAWaffle");
		s.addFriendship("PrayUp", "NeverMaxine");
		s.addFriendship("NerdChief", "NeverMaxine");
		
		s.addGroup("Prayer Group");
		s.addGroup("Mom 'N Daughter");
		s.addGroup("Going to LA");
		s.addGroup("Arrrrgh Pirates");
		s.addGroup("Students");
		
		assertTrue(s.listOfGroups().contains("Prayer Group"));
		assertTrue(s.listOfGroups().contains("Mom 'N Daughter"));
		assertTrue(s.listOfGroups().contains("Going to LA"));
		assertTrue(s.listOfGroups().contains("Arrrrgh Pirates"));
		assertTrue(s.listOfGroups().contains("Students"));
		
		s.addPersonToGroup("PrayUp", "Prayer Group");
		s.addPersonToGroup("NeverMaxine", "Prayer Group");
		s.addPersonToGroup("FancyAWaffle", "Prayer Group");
		
		s.addPersonToGroup("TotallyNotChloe", "Going to LA");
		s.addPersonToGroup("AcherRambel", "Going to LA");
		
		s.addPersonToGroup("NeverMaxine", "Arrrrgh Pirates");
		s.addPersonToGroup("TotallyNotChloe", "Arrrrgh Pirates");
		
		s.addPersonToGroup("TotallyNotChloe", "Students");
		s.addPersonToGroup("NeverMaxine", "Students");
		s.addPersonToGroup("PrayUp", "Students");
		s.addPersonToGroup("AcherRambel", "Students");
	}
	
	@Test
	public void testR4_Stats() throws NoSuchCodeException {
		
		assertEquals("TotallyNotChloe", s.personWithLargestNumberOfFriends());
		assertEquals("NerdChief", s.personWithMostFriendsOfFriends());
		
		assertEquals("Students", s.largestGroup());
		
		assertEquals("TotallyNotChloe", s.personInLargestNumberOfGroups());
	}

}
