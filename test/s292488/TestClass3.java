package s292488;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class TestClass3 {
	
	Social s;
	
	@Before
	public void setUp() throws PersonExistsException {
		s = new Social();
		s.addPerson("NeverMaxine", "Max", "Caulfield");
		s.addPerson("TotallyNotChloe", "Chloe", "Price");
		s.addPerson("AcherRambel", "Rachel", "Amber");
		s.addPerson("FancyAWaffle", "Joyce", "Price-Madsen");
		s.addPerson("PrayUp", "Kate", "Marsh");
	}
	
	@Test
	public void testR3_Groups() throws NoSuchCodeException {
		s.addGroup("Prayer Group");
		s.addGroup("Mom 'N Daughter");
		s.addGroup("Going to LA");
		s.addGroup("Arrrrgh Pirates");
		s.addGroup("Students");
		
		assertTrue(s.listOfGroups().contains("Prayer Group"));
		assertTrue(s.listOfGroups().contains("Mom 'N Daughter"));
		assertTrue(s.listOfGroups().contains("Going to LA"));
		assertTrue(s.listOfGroups().contains("Arrrrgh Pirates"));
		assertTrue(s.listOfGroups().contains("Students"));
		
		s.addPersonToGroup("PrayUp", "Prayer Group");
		s.addPersonToGroup("NeverMaxine", "Prayer Group");
		s.addPersonToGroup("FancyAWaffle", "Prayer Group");
		
		s.addPersonToGroup("TotallyNotChloe", "Going to LA");
		s.addPersonToGroup("AcherRambel", "Going to LA");
		
		s.addPersonToGroup("NeverMaxine", "Arrrrgh Pirates");
		s.addPersonToGroup("TotallyNotChloe", "Arrrrgh Pirates");
		
		s.addPersonToGroup("TotallyNotChloe", "Students");
		s.addPersonToGroup("NeverMaxine", "Students");
		s.addPersonToGroup("PrayUp", "Students");
		s.addPersonToGroup("AcherRambel", "Students");
		
		assertTrue(s.listOfPeopleInGroup("Students").contains("NeverMaxine"));
		assertTrue(s.listOfPeopleInGroup("Students").contains("TotallyNotChloe"));
		assertTrue(s.listOfPeopleInGroup("Students").contains("PrayUp"));
		assertTrue(s.listOfPeopleInGroup("Students").contains("AcherRambel"));
		
		assertTrue(s.listOfPeopleInGroup("Prayer Group").contains("NeverMaxine"));
		assertTrue(s.listOfPeopleInGroup("Prayer Group").contains("FancyAWaffle"));
		assertTrue(s.listOfPeopleInGroup("Prayer Group").contains("PrayUp"));
		
		assertTrue(s.listOfPeopleInGroup("Arrrrgh Pirates").contains("NeverMaxine"));
		assertTrue(s.listOfPeopleInGroup("Arrrrgh Pirates").contains("TotallyNotChloe"));
		
		assertTrue(s.listOfPeopleInGroup("Going to LA").contains("AcherRambel"));
		assertTrue(s.listOfPeopleInGroup("Going to LA").contains("TotallyNotChloe"));
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR3_NoSuchCode() throws NoSuchCodeException {
		s.addGroup("Prayer Group");
		s.addGroup("Mom 'N Daughter");
		s.addGroup("Going to LA");
		s.addGroup("Arrrrgh Pirates");
		s.addGroup("Students");
		
		s.addPersonToGroup("VictoriaChase", "Students");
	}

}
