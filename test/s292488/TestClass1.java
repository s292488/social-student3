package s292488;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class TestClass1 {
	
	Social s;
	
	@Before
	public void setUp() {
		s = new Social();
	}
	
	@Test
	public void testR1_Subscription() throws PersonExistsException, NoSuchCodeException {
		s.addPerson("NeverMaxine", "Max", "Caulfield");
		s.addPerson("TotallyNotChloe", "Chloe", "Price");
		s.addPerson("AcherRambel", "Rachel", "Amber");
		s.addPerson("FancyAWaffle", "Joyce", "Price-Madsen");
		s.addPerson("PrayUp", "Kate", "Marsh");
		
		assertEquals("NeverMaxine Max Caulfield", s.getPerson("NeverMaxine"));
		assertEquals("TotallyNotChloe Chloe Price", s.getPerson("TotallyNotChloe"));
		assertEquals("AcherRambel Rachel Amber", s.getPerson("AcherRambel"));
		assertEquals("FancyAWaffle Joyce Price-Madsen", s.getPerson("FancyAWaffle"));
		assertEquals("PrayUp Kate Marsh", s.getPerson("PrayUp"));
	}
	
	@Test(expected = PersonExistsException.class)
	public void testR1_PersonAlreadyPresent() throws PersonExistsException {
		s.addPerson("NeverMaxine", "Max", "Caulfield");
		s.addPerson("NeverMaxine", "Max", "Mayfield");
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR1_WrongCode() throws PersonExistsException, NoSuchCodeException {
		s.addPerson("NeverMaxine", "Max", "Caulfield");
		s.addPerson("TotallyNotChloe", "Chloe", "Price");
		s.addPerson("AcherRambel", "Rachel", "Amber");
		s.addPerson("FancyAWaffle", "Joyce", "Price-Madsen");
		s.addPerson("PrayUp", "Kate", "Marsh");
		
		s.getPerson("IHaveNeverSeenThisManInMyLife");
	}
}
