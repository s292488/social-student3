package s292488;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class TestClass2 {
	
	Social s;
	
	@Before
	public void setUp() throws PersonExistsException {
		s = new Social();
		s.addPerson("NeverMaxine", "Max", "Caulfield");
		s.addPerson("TotallyNotChloe", "Chloe", "Price");
		s.addPerson("AcherRambel", "Rachel", "Amber");
		s.addPerson("FancyAWaffle", "Joyce", "Price-Madsen");
		s.addPerson("PrayUp", "Kate", "Marsh");
	}
	
	@Test
	public void testR2_Friends() throws NoSuchCodeException {
		s.addFriendship("NeverMaxine", "TotallyNotChloe");
		s.addFriendship("AcherRambel", "TotallyNotChloe");
		s.addFriendship("TotallyNotChloe", "FancyAWaffle");
		s.addFriendship("PrayUp", "NeverMaxine");
		
		assertTrue(s.listOfFriends("NeverMaxine").contains("PrayUp"));
		assertTrue(s.listOfFriends("NeverMaxine").contains("TotallyNotChloe"));
		assertTrue(s.listOfFriends("AcherRambel").contains("TotallyNotChloe"));
		assertTrue(s.listOfFriends("TotallyNotChloe").contains("AcherRambel"));
		
		assertTrue(s.friendsOfFriends("NeverMaxine").contains("AcherRambel"));
		assertTrue(s.friendsOfFriends("TotallyNotChloe").contains("PrayUp"));
		assertFalse(s.friendsOfFriends("PrayUp").contains("PrayUp"));
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR2_NoSuchCodeListOfFriends() throws NoSuchCodeException {
		s.addFriendship("NeverMaxine", "TotallyNotChloe");
		s.addFriendship("AcherRambel", "TotallyNotChloe");
		s.addFriendship("TotallyNotChloe", "FancyAWaffle");
		s.addFriendship("PrayUp", "NeverMaxine");
		
		s.listOfFriends("MaybeMaxineIsFine");
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR2_NoSuchCodeFriendsOfFriends() throws NoSuchCodeException {
		s.addFriendship("NeverMaxine", "TotallyNotChloe");
		s.addFriendship("AcherRambel", "TotallyNotChloe");
		s.addFriendship("TotallyNotChloe", "FancyAWaffle");
		s.addFriendship("PrayUp", "NeverMaxine");
		
		s.friendsOfFriends("MaybeMaxineIsFine");
	}

}
